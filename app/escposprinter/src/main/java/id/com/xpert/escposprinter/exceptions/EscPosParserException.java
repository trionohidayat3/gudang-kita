package id.com.xpert.escposprinter.exceptions;

public class EscPosParserException extends Exception {
    public EscPosParserException(String errorMessage) {
        super(errorMessage);
    }
}
