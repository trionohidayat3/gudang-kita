package id.com.xpert.escposprinter.exceptions;

public class EscPosEncodingException extends Exception {
    public EscPosEncodingException(String errorMessage) {
        super(errorMessage);
    }
}
