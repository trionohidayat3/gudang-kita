package id.com.xpert.escposprinter.exceptions;

public class EscPosBarcodeException extends Exception {
    public EscPosBarcodeException(String errorMessage) {
        super(errorMessage);
    }
}
