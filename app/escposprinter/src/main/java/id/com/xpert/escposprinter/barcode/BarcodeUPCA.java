package id.com.xpert.escposprinter.barcode;

import id.com.xpert.escposprinter.EscPosPrinterCommands;
import id.com.xpert.escposprinter.EscPosPrinterSize;
import id.com.xpert.escposprinter.exceptions.EscPosBarcodeException;

public class BarcodeUPCA extends BarcodeNumber {

    public BarcodeUPCA(EscPosPrinterSize printerSize, String code, float widthMM, float heightMM, int textPosition) throws EscPosBarcodeException {
        super(printerSize, EscPosPrinterCommands.BARCODE_TYPE_UPCA, code, widthMM, heightMM, textPosition);
    }

    @Override
    public int getCodeLength() {
        return 12;
    }
}
