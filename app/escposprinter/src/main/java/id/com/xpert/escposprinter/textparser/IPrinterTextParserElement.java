package id.com.xpert.escposprinter.textparser;

import id.com.xpert.escposprinter.EscPosPrinterCommands;
import id.com.xpert.escposprinter.exceptions.EscPosEncodingException;

public interface IPrinterTextParserElement {
    int length();
    IPrinterTextParserElement print(EscPosPrinterCommands printerSocket) throws EscPosEncodingException;
}
