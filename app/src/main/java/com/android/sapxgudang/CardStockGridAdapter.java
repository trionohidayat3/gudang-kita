package com.android.sapxgudang;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.sapxgudang.activity.DetailStockActivity;

import java.util.List;

public class CardStockGridAdapter extends RecyclerView.Adapter<CardStockGridAdapter.ViewHolder> {
    List<CardStock> cardStockList;
    Context context;

    public CardStockGridAdapter(List<CardStock> cardStockList, Context context) {
        this.cardStockList = cardStockList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_grid_stock, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CardStock stock = cardStockList.get(position);
        holder.tv_kode_produk.setText(stock.getKodeProduk());
        holder.tv_stok_saldo.setText(stock.getSaldo());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailStockActivity.class);
            intent.putExtra("KODE_PRODUK", holder.tv_kode_produk.getText());
//            intent.putExtra("STOK_PRODUK", holder.tv_stok_saldo.getText());
//            intent.putExtra("TANGGAL_STOK", stock.getTanggal());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return Math.min(cardStockList.size(), 4);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_kode_produk, tv_stok_saldo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_kode_produk = itemView.findViewById(R.id.tv_kode_produk);
            tv_stok_saldo = itemView.findViewById(R.id.tv_stok_saldo);
        }
    }
}
