package com.android.sapxgudang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardStock {

    @SerializedName("kode_produk")
    @Expose
    private String kodeProduk;

    @SerializedName("masuk")
    @Expose
    private String masuk;

    @SerializedName("keluar")
    @Expose
    private String keluar;

    @SerializedName("saldo")
    @Expose
    private String saldo;

    @SerializedName("ref_no")
    @Expose
    private String refNo;

    @SerializedName("tanggal")
    @Expose
    private String tanggal;

    public String getKodeProduk() {
        return kodeProduk;
    }

    public void setKodeProduk(String kodeProduk) {
        this.kodeProduk = kodeProduk;
    }

    public String getMasuk() {
        return masuk;
    }

    public void setMasuk(String masuk) {
        this.masuk = masuk;
    }

    public String getKeluar() {
        return keluar;
    }

    public void setKeluar(String keluar) {
        this.keluar = keluar;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

}
