package com.android.sapxgudang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductIncome {

    @SerializedName("no_masuk")
    @Expose
    String no_masuk;

    @SerializedName("kode_produk")
    @Expose
    String kode_produk;

    @SerializedName("jumlah")
    @Expose
    String jumlah;

    @SerializedName("tanggal")
    @Expose
    String tanggal;

    public String getNo_masuk() {
        return no_masuk;
    }

    public void setNo_masuk(String no_masuk) {
        this.no_masuk = no_masuk;
    }

    public String getKode_produk() {
        return kode_produk;
    }

    public void setKode_produk(String kode_produk) {
        this.kode_produk = kode_produk;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
