package com.android.sapxgudang.fragment.warehouse;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.Order;
import com.android.sapxgudang.ProductOutcome;
import com.android.sapxgudang.ProductOutcomeAdapter;
import com.android.sapxgudang.R;
import com.android.sapxgudang.OutcomingProductActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarehouseKeluarFragment extends Fragment {

    SwipeRefreshLayout swiperefresh_layout;
    RecyclerView rv_produk_keluar;
    ProductOutcomeAdapter adapter;

    FloatingActionButton fab_warehouse_incoming;

    SimpleDateFormat dateFormat;
    Calendar calendar;

    AutoCompleteTextView auto_no_pesanan;
    TextInputEditText txt_kode_barang, txt_jumlah_barang;

    API_Product api_product;

    List<Order> orderList;
    List<ProductOutcome> productOutcomeList;

    public WarehouseKeluarFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_warehouse_outcoming, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        swiperefresh_layout = view.findViewById(R.id.swiperefresh_layout);
        swiperefresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swiperefresh_layout.setOnRefreshListener(() -> {
            swiperefresh_layout.setRefreshing(false);
            loaddata();
        });
        rv_produk_keluar = view.findViewById(R.id.rv_produk_keluar);
        rv_produk_keluar.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_produk_keluar.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        api_product = API_Client.getClientProduct().create(API_Product.class);
        loaddata();

        fab_warehouse_incoming = view.findViewById(R.id.fab_warehouse_incoming);
        fab_warehouse_incoming.setOnClickListener(view1 -> startActivity(new Intent(getActivity(), OutcomingProductActivity.class)));
//        fab_warehouse_incoming.setOnClickListener(view1 -> dialogProductOutcoming());
    }

    private void loaddata() {
        Call<List<ProductOutcome>> call = api_product.view_outcoming_stock();
        call.enqueue(new Callback<List<ProductOutcome>>() {
            @Override
            public void onResponse(Call<List<ProductOutcome>> call, Response<List<ProductOutcome>> response) {
                productOutcomeList = response.body();
                adapter = new ProductOutcomeAdapter(productOutcomeList, getContext());
                rv_produk_keluar.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ProductOutcome>> call, Throwable t) {

            }
        });
    }

    private void dialogProductOutcoming() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_product_outcoming);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.bt_close).setOnClickListener(v -> dialog.dismiss());

        auto_no_pesanan = dialog.findViewById(R.id.auto_no_pesanan);
        txt_kode_barang = dialog.findViewById(R.id.txt_kode_barang);
        txt_jumlah_barang = dialog.findViewById(R.id.txt_jumlah_barang);

        api_product = API_Client.getClientProduct().create(API_Product.class);

        Call<List<Order>> call = api_product.view_order();
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                orderList = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(orderList).size(); i++) {
                    listAuto.add(orderList.get(i).getNoOrderPesanan());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                auto_no_pesanan.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {

            }
        });

        auto_no_pesanan.setOnItemClickListener((adapterView, view, i, l) -> {
            Order order = orderList.get(i);
            txt_kode_barang.setText(order.getKodeProduk());
            txt_jumlah_barang.setText(order.getJumlah());
        });

        dialog.findViewById(R.id.bt_save).setOnClickListener(v -> {
            String tanggal_jam = dateFormat.format(calendar.getTime());
            String no_pesanan = auto_no_pesanan.getText().toString();
            String kode_barang = txt_kode_barang.getText().toString();
            String jumlah_barang = txt_jumlah_barang.getText().toString();

            api_product = API_Client.getClientProduct().create(API_Product.class);

            Call<ResponseBody> call2 = api_product.insert_outcoming_stock(no_pesanan, kode_barang, jumlah_barang, tanggal_jam);
            call2.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Toast.makeText(getContext(), "Berhasil", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

            Call<ResponseBody> call_card_stock_incoming = api_product.insert_card_stock_outcoming(kode_barang, "0", jumlah_barang, no_pesanan, tanggal_jam);
            call_card_stock_incoming.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.e("Log: ", String.valueOf(response));
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


}