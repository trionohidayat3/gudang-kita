package com.android.sapxgudang.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;

import okhttp3.ResponseBody;


import com.android.sapxgudang.Product;
import com.android.sapxgudang.ProductAdapter;
import com.android.sapxgudang.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductFragment extends Fragment {

    SwipeRefreshLayout swiperefresh_layout;
    RecyclerView rv_produk;
    ArrayList<Product> productList;
    ProductAdapter adapter;


    FloatingActionButton fab_tambah_barang;
    String kode_barang, nama_barang;

    API_Product api_product;

    public ProductFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        swiperefresh_layout = view.findViewById(R.id.swiperefresh_layout);
        swiperefresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swiperefresh_layout.setOnRefreshListener(() -> {
            swiperefresh_layout.setRefreshing(false);
            loaddata();
        });
        rv_produk = view.findViewById(R.id.rv_produk);
        rv_produk.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_produk.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        api_product = API_Client.getClientProduct().create(API_Product.class);

        loaddata();


        fab_tambah_barang = view.findViewById(R.id.fab_tambah_barang);
        fab_tambah_barang.setOnClickListener(view1 -> dialogTambahBarang());

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search_product, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);

        SearchView searchView = new SearchView(getContext());
        searchView.setQueryHint("Tulis Kode Produk");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String nextText) {
                nextText = nextText.toUpperCase();
                ArrayList<Product> dataFilter = new ArrayList<>();
                for (Product data : productList) {
                    String nama = data.getKode_produk().toUpperCase();
                    if (nama.contains(nextText)) {
                        dataFilter.add(data);
                    }
                }
                adapter.setFilter(dataFilter);
                return true;
            }
        });
//        super.onCreateOptionsMenu(menu, inflater);
        searchItem.setActionView(searchView);

    }

    private void loaddata() {
        Call<ArrayList<Product>> call = api_product.view_product();
        call.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Product>> call, @NonNull Response<ArrayList<Product>> response) {
                productList = response.body();
                adapter = new ProductAdapter(productList);
                rv_produk.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Product>> call, @NonNull Throwable t) {

            }
        });
    }

    private void dialogTambahBarang() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_product);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextInputEditText txt_kode_barang = dialog.findViewById(R.id.txt_kode_barang);
        final TextInputEditText txt_nama_barang = dialog.findViewById(R.id.txt_nama_barang);

        dialog.findViewById(R.id.bt_close).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.bt_save).setOnClickListener(v -> {

            kode_barang = Objects.requireNonNull(txt_kode_barang.getText()).toString().trim();
            nama_barang = Objects.requireNonNull(txt_nama_barang.getText()).toString().trim();

            if (kode_barang.length() == 0) {
                txt_kode_barang.setError("Masukkan Kode Barang");
            } else if (nama_barang.length() == 0) {
                txt_nama_barang.setError("Masukkan Nama Barang");
            } else {


                api_product = API_Client.getClientProduct().create(API_Product.class);

                Call<ResponseBody> call = api_product.insert_product(kode_barang, nama_barang);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                        Toast.makeText(getContext(), "Produk berhasil di tambahkan", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                        Toast.makeText(getContext(), "Jaringan Error!", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();
            }
            loaddata();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}