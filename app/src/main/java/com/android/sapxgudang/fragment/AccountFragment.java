package com.android.sapxgudang.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.R;
import com.android.sapxgudang.SharedPrefManager;
import com.android.sapxgudang.User;
import com.android.sapxgudang.activity.StartedActivity;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountFragment extends Fragment {

    SharedPrefManager sharedPrefManager;

    API_Product apiProduct;

    List<User> userList;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPrefManager = new SharedPrefManager(getContext());

        TextView textNameUser = view.findViewById(R.id.text_name_user);

        apiProduct = API_Client.getClientProduct().create(API_Product.class);
        Call<List<User>> call = apiProduct.view_user();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                userList = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(userList).size(); i++) {
                    listAuto.add(userList.get(i).getColNama());
                    textNameUser.setText(userList.get(i).getColNama());
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });

        MaterialCardView card_logout = view.findViewById(R.id.card_logout);
        card_logout.setOnClickListener(view1 -> {
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            startActivity(new Intent(getContext(), StartedActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            getActivity().finish();
        });
    }
}