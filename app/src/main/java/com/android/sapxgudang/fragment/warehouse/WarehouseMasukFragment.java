package com.android.sapxgudang.fragment.warehouse;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.Product;
import com.android.sapxgudang.ProductAdapter;
import com.android.sapxgudang.ProductIncome;
import com.android.sapxgudang.ProductIncomeAdapter;
import com.android.sapxgudang.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarehouseMasukFragment extends Fragment {

    SwipeRefreshLayout swiperefresh_layout;
    RecyclerView rv_produk_masuk;
    ProductIncomeAdapter incomeAdapter;

    FloatingActionButton fab_warehouse_incoming;

    SimpleDateFormat dateFormat;
    Calendar calendar;

    AutoCompleteTextView auto_kode_barang;
    TextInputEditText txt_no_masuk, txt_nama_barang, txt_jumlah_barang;

    API_Product api_product;

    List<Product> productList;
    List<ProductIncome> productIncomeList;

    public WarehouseMasukFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_warehouse_incoming, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        swiperefresh_layout = view.findViewById(R.id.swiperefresh_layout);
        swiperefresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swiperefresh_layout.setOnRefreshListener(() -> {
            swiperefresh_layout.setRefreshing(false);
            loaddata();
        });

        rv_produk_masuk = view.findViewById(R.id.rv_produk_masuk);
        rv_produk_masuk.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_produk_masuk.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        api_product = API_Client.getClientProduct().create(API_Product.class);

        loaddata();

        fab_warehouse_incoming = view.findViewById(R.id.fab_warehouse_incoming);
        fab_warehouse_incoming.setOnClickListener(view1 -> dialogProductIncoming());

    }

    private void loaddata() {
        Call<List<ProductIncome>> call = api_product.view_incoming_stock();
        call.enqueue(new Callback<List<ProductIncome>>() {
            @Override
            public void onResponse(Call<List<ProductIncome>> call, Response<List<ProductIncome>> response) {
                productIncomeList = response.body();
                incomeAdapter = new ProductIncomeAdapter(productIncomeList, getContext());
                rv_produk_masuk.setAdapter(incomeAdapter);
            }

            @Override
            public void onFailure(Call<List<ProductIncome>> call, Throwable t) {

            }
        });
    }

    private void dialogProductIncoming() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_product_incoming);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.bt_close).setOnClickListener(v -> dialog.dismiss());

        auto_kode_barang = dialog.findViewById(R.id.auto_kode_barang);
        txt_no_masuk = dialog.findViewById(R.id.txt_no_masuk);
        txt_nama_barang = dialog.findViewById(R.id.txt_nama_barang);
        txt_jumlah_barang = dialog.findViewById(R.id.txt_jumlah_barang);

        api_product = API_Client.getClientProduct().create(API_Product.class);

        Call<List<Product>> call = api_product.view_product_warehouse();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                productList = response.body();
                List<String> listAuto = new ArrayList<>();
                for (int i = 0; i < Objects.requireNonNull(productList).size(); i++) {
                    listAuto.add(productList.get(i).getKode_produk());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, listAuto);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                auto_kode_barang.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable t) {

            }
        });

        auto_kode_barang.setOnItemClickListener((adapterView, view, i, l) -> {
            Product product = productList.get(i);
            txt_nama_barang.setText(product.getNama_produk());

            ProductIncome productIncome = productIncomeList.get(i);
            txt_no_masuk.setText(productIncome.getNo_masuk());
        });

        dialog.findViewById(R.id.bt_save).setOnClickListener(v -> {
            String tanggal_jam = dateFormat.format(calendar.getTime());
            String kode_barang = auto_kode_barang.getText().toString();
            String jumlah_barang = txt_jumlah_barang.getText().toString();
            String no_masuk = txt_no_masuk.getText().toString();

            if (kode_barang.length() == 0) {
                auto_kode_barang.setError("Pilih Barang");
            } else if (jumlah_barang.length() == 0) {
                txt_jumlah_barang.setError("Masukkan Jumlah");
            } else {
                auto_kode_barang.setError(null);
                txt_jumlah_barang.setError(null);

                api_product = API_Client.getClientProduct().create(API_Product.class);

                Call<ResponseBody> call2 = api_product.insert_incoming_stock(kode_barang, jumlah_barang, tanggal_jam);
                call2.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(getContext(), "Berhasil", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                Call<ResponseBody> call_card_stock_incoming = api_product.insert_card_stock_incoming(kode_barang, jumlah_barang, "0", no_masuk, tanggal_jam);
                call_card_stock_incoming.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.e("Log: ", String.valueOf(response));
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                dialog.dismiss();
            }

            loaddata();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}