package com.android.sapxgudang.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.activity.AllProductActivity;
import com.android.sapxgudang.CardStock;
import com.android.sapxgudang.CardStockGridAdapter;
import com.android.sapxgudang.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockFragment extends Fragment {

    SwipeRefreshLayout swiperefresh_layout;
    Button btn_all_product;
    RecyclerView rv_all_product, rv_low_stock, rv_best_selling;
    List<CardStock> cardStockList;
    //    CardStockListAdapter adapter;
    CardStockGridAdapter adapter;

    API_Product api_product;

    public StockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stock, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swiperefresh_layout = view.findViewById(R.id.swiperefresh_layout);
        swiperefresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swiperefresh_layout.setOnRefreshListener(() -> {
            swiperefresh_layout.setRefreshing(false);
            loaddata();
        });

        btn_all_product = view.findViewById(R.id.btn_all_product);
        btn_all_product.setOnClickListener(view1 -> startActivity(new Intent(getContext(), AllProductActivity.class)));

        rv_all_product = view.findViewById(R.id.rv_all_product);
        rv_all_product.setHasFixedSize(true);
        rv_all_product.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        api_product = API_Client.getClientProduct().create(API_Product.class);

        loaddata();

    }

    private void loaddata() {

        //All Stock
        Call<List<CardStock>> call = api_product.view_card_stock();
        call.enqueue(new Callback<List<CardStock>>() {
            @Override
            public void onResponse(@NonNull Call<List<CardStock>> call, @NonNull Response<List<CardStock>> response) {
                cardStockList = response.body();
                adapter = new CardStockGridAdapter(cardStockList, getContext());
                rv_all_product.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<CardStock>> call, @NonNull Throwable t) {

            }
        });

        //Low Stock


        //Best Selling
    }

}