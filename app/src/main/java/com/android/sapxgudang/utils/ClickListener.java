package com.android.sapxgudang.utils;

import android.view.View;

public interface ClickListener {

    void OnClick(View view, int posisi);
    void onLongClick(View view, int posisi);
}
