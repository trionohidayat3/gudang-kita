package com.android.sapxgudang.api;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiData_Order_Pesanan {



    @GET("order_pesanan_api/get_order_pesanan_expedisi?")
    Call<APIgsonList> doGetByexpedisi(@Query("db_otp") String db_otp,
                                                    @Query("email") String email,
                                                    @Query("password") String password,
                                                    @Query("expedisi") String expedisi);

    @FormUrlEncoded
    @POST("order_pesanan_api/update_status?")
    Call<APIgsonList> doUpdateStatusOrderPesananField(@Query("db_otp") String db_otp,
                                                          @Query("email") String email,
                                                          @Query("password") String password,
                                                          @Field("order_pesan") String order_pesan);
}
