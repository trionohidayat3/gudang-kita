package com.android.sapxgudang.api;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiData_Reseller {

    @FormUrlEncoded
    @POST("reseller_api/register?")
    Call<APIgsonList> doCreateActionResellerField(@Query("db_otp") String db_otp,
                                                  @Query("email") String email,
                                                  @Query("password") String password,
                                                  @Field("pic") String pic,
                                                  @Field("reseller") String reseller,
                                                  @Field("phone") String phone);


    @GET("reseller_api/alamatpengiriman?")
    Call<APIgsonList> doGetData_AlamatPengiriman(@Query("db_otp") String db_otp,
                                                 @Query("id_reseller") String id_reseller,@Query("qs") String qs);

    @GET("reseller_api/pengirim?")
    Call<APIgsonList> doGetData_Pengirim(@Query("db_otp") String db_otp,@Query("qs") String qs);

    @GET("reseller_api/rekening_bank?")
    Call<APIgsonList> doGetData_Rekening_bank(@Query("db_otp") String db_otp,@Query("qs") String qs);

    @GET("reseller_api/get_reseller_info?")
    Call<APIgsonList> doGetData_Reseller_info(@Query("db_otp") String db_otp,
                                              @Query("email") String email,
                                              @Query("password") String password);

    @Multipart
    @POST("reseller_api/uploadfile_logo?")
    Call<APIgsonList> doGetData_insertLogo(@Query("db_otp") String db_otp,
                                           @Query("email") String email,
                                           @Query("password") String password,
                                           @Query("id_reseller") String id_reseller,
                                           @Part MultipartBody.Part file, @Part("file") RequestBody name);


}
