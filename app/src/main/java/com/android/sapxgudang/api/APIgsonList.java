package com.android.sapxgudang.api;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class APIgsonList {

    @SerializedName("page")
    public Integer page;
    @SerializedName("per_page")
    public Integer perPage;
    @SerializedName("total_rows")
    public Integer total_rows;
    @SerializedName("total_pages")
    public Integer totalPages;

    @SerializedName("success")
    public Integer success;
    @SerializedName("message")
    public String  message;

    @SerializedName("nomor")
    public String  nomor;

    @SerializedName("id_response")
    public String  id_response;

    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {

        @SerializedName("id")
        public String id;

        @SerializedName("kol_01")
        public String kol_01;

        @SerializedName("kol_02")
        public String kol_02;

        @SerializedName("kol_03")
        public String kol_03;

        @SerializedName("kol_04")
        public String kol_04;

        @SerializedName("kol_05")
        public String kol_05;

        @SerializedName("kol_06")
        public String kol_06;

        @SerializedName("kol_07")
        public String kol_07;

        @SerializedName("kol_08")
        public String kol_08;

        @SerializedName("kol_09")
        public String kol_09;

        @SerializedName("kol_10")
        public String kol_10;

        @SerializedName("kol_11")
        public String kol_11;

        @SerializedName("kol_12")
        public String kol_12;

        @SerializedName("kol_13")
        public String kol_13;

        @SerializedName("kol_14")
        public String kol_14;

        @SerializedName("kol_15")
        public String kol_15;

        @SerializedName("kol_16")
        public String kol_16;

        @SerializedName("kol_17")
        public String kol_17;

        @SerializedName("kol_18")
        public String kol_18;

        @SerializedName("kol_19")
        public String kol_19;

        @SerializedName("kol_20")
        public String kol_20;

        @SerializedName("kol_21")
        public String kol_21;

        @SerializedName("kol_22")
        public String kol_22;

        @SerializedName("kol_23")
        public String kol_23;

        @SerializedName("kol_24")
        public String kol_24;

        @SerializedName("kol_25")
        public String kol_25;

        @SerializedName("kol_26")
        public String kol_26;

        @SerializedName("kol_27")
        public String kol_27;

        @SerializedName("kol_28")
        public String kol_28;

        @SerializedName("kol_29")
        public String kol_29;

        @SerializedName("kol_30")
        public String kol_30;

        @SerializedName("kol_31")
        public String kol_31;

        @SerializedName("kol_32")
        public String kol_32;

        @SerializedName("kol_33")
        public String kol_33;

        @SerializedName("kol_34")
        public String kol_34;

        @SerializedName("kol_35")
        public String kol_35;

    }

}
