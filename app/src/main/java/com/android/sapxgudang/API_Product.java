package com.android.sapxgudang;


import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API_Product {

    @FormUrlEncoded
    @POST("insert_user.php")
    Call<ResponseBody> insert_user(@Field("col_nama") String col_nama,
                                   @Field("col_email") String col_email,
                                   @Field("col_password") String col_password);

    @GET("login_user.php")
    Call<List<User>> login_user(@Query("col_email") String col_email,
                                  @Query("col_password") String col_password);

    @GET("view_user.php")
    Call<List<User>> view_user();

    @FormUrlEncoded
    @POST("insert_product.php")
    Call<ResponseBody> insert_product(@Field("kode_produk") String kode_produk,
                                      @Field("nama_produk") String nama_produk);

    @GET("view_product.php")
    Call<ArrayList<Product>> view_product();


    @GET("view_product.php")
    Call<List<Product>> view_product_warehouse();


    @FormUrlEncoded
    @POST("insert_incoming_stock.php")
    Call<ResponseBody> insert_incoming_stock(@Field("kode_produk") String kode_produk,
                                             @Field("jumlah") String jumlah,
                                             @Field("tanggal") String tanggal);


    @GET("view_incoming_stock.php")
    Call<List<ProductIncome>> view_incoming_stock();

    @FormUrlEncoded
    @POST("insert_outcoming_stock.php")
    Call<ResponseBody> insert_outcoming_stock(@Field("no_order_pesanan") String no_order_pesanan,
                                              @Field("kode_produk") String kode_produk,
                                              @Field("jumlah") String jumlah,
                                              @Field("tanggal") String tanggal);

    @GET("view_outcoming_stock.php")
    Call<List<ProductOutcome>> view_outcoming_stock();


    @FormUrlEncoded
    @POST("insert_card_stock_incoming.php")
    Call<ResponseBody> insert_card_stock_incoming(@Field("kode_produk") String kode_produk,
                                                  @Field("masuk") String masuk,
                                                  @Field("keluar") String keluar,
                                                  @Field("ref_no") String ref_no,
                                                  @Field("tanggal") String tanggal);

    @FormUrlEncoded
    @POST("insert_card_stock_outcoming.php")
    Call<ResponseBody> insert_card_stock_outcoming(@Field("kode_produk") String kode_produk,
                                                   @Field("masuk") String masuk,
                                                   @Field("keluar") String keluar,
                                                   @Field("ref_no") String ref_no,
                                                   @Field("tanggal") String tanggal);

    @GET("view_card_stock.php")
    Call<List<CardStock>> view_card_stock();


    @GET("view_specific_stock.php?")
    Call<List<CardStock>> view_specific_stock(@Query("kode_produk") String kode_produk);

    @GET("view_order.php")
    Call<List<Order>> view_order();


    @GET("view_specific_stock_product.php")
    Call<List<Order>> view_specific_stock_product(@Query("kode_produk") String kode_produk);
}
