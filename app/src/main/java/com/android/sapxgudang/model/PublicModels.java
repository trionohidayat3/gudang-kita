package com.android.sapxgudang.model;
import java.io.Serializable;

public class PublicModels implements Serializable {
    private int norut;

    private String id;
    private String kol_01;
    private String kol_02;
    private String kol_03;
    private String kol_04;
    private String kol_05;
    private String kol_06;
    private String kol_07;
    private String kol_08;
    private String kol_09;
    private String kol_10;
    private String kol_11;
    private String kol_12;
    private String kol_13;
    private String kol_14;
    private String kol_15;
    private String kol_16;
    private String kol_17;
    private String kol_18;
    private String kol_19;
    private String kol_20;

    public PublicModels(int norut
            , String id
            , String kol_01
            , String kol_02
            , String kol_03
            , String kol_04
            , String kol_05
            , String kol_06
            , String kol_07
            , String kol_08
            , String kol_09
            , String kol_10
            , String kol_11
            , String kol_12
            , String kol_13
            , String kol_14
            , String kol_15
            , String kol_16
            , String kol_17
            , String kol_18
            , String kol_19
            , String kol_20) {

        this.norut=norut;
        this.id=id;
        this.kol_01=kol_01;
        this.kol_02=kol_02;
        this.kol_03=kol_03;
        this.kol_04=kol_04;
        this.kol_05=kol_05;
        this.kol_06=kol_06;
        this.kol_07=kol_07;
        this.kol_08=kol_08;
        this.kol_09=kol_09;
        this.kol_10=kol_10;
        this.kol_11=kol_11;
        this.kol_12=kol_12;
        this.kol_13=kol_13;
        this.kol_14=kol_14;
        this.kol_15=kol_15;
        this.kol_16=kol_16;
        this.kol_17=kol_17;
        this.kol_18=kol_18;
        this.kol_19=kol_19;
        this.kol_20=kol_20;
    }

    public int getNorut() {
        return norut;
    }

    public void setNorut(int norut) {
        this.norut = norut;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id;  }

    public String getKol_01() {
        return kol_01;
    }

    public void setKol_01(String kol_01) { this.kol_01 = kol_01;  }

    public String getKol_02() {
        return kol_02;
    }

    public void setKol_02(String kol_02) {
        this.kol_02 = kol_02;
    }

    public String getKol_03() {  return kol_03;   }

    public void setKol_03(String kol_03) {
        this.kol_03 = kol_03;
    }

    public String getKol_04() { return kol_04;  }

    public void setKol_04(String kol_04) {
        this.kol_04 = kol_04;
    }

    public String getKol_05() {
        return kol_05;
    }

    public void setKol_05(String kol_05) {
        this.kol_05 = kol_05;
    }

    public String getKol_06() {
        return kol_06;
    }

    public void setKol_06(String kol_06) {
        this.kol_06 = kol_06;
    }

    public String getKol_07() {
        return kol_07;
    }

    public void setKol_07(String kol_07) {
        this.kol_07 = kol_07;
    }

    public String getKol_08() {
        return kol_08;
    }

    public void setKol_08(String kol_08) {
        this.kol_08 = kol_08;
    }

    public String getKol_09() {
        return kol_09;
    }

    public void setKol_09(String kol_09) {
        this.kol_09 = kol_09;
    }

    public String getKol_10() {
        return kol_10;
    }

    public void setKol_10(String kol_10) {
        this.kol_10 = kol_10;
    }

    public String getKol_11() {
        return kol_11;
    }

    public void setKol_11(String kol_11) {
        this.kol_11= kol_11;
    }

    public String getKol_12() {
        return kol_12;
    }

    public void setKol_12(String kol_12) {
        this.kol_12 = kol_12;
    }

    public String getKol_13() {
        return kol_13;
    }

    public void setKol_13(String kol_13) {
        this.kol_13 = kol_13;
    }

    public String getKol_14() {
        return kol_14;
    }

    public void setKol_14(String kol_14) {
        this.kol_14 = kol_14;
    }

    public String getKol_15() {
        return kol_15;
    }

    public void setKol_15(String kol_15) {
        this.kol_15 = kol_15;
    }

    public String getKol_16() {
        return kol_16;
    }

    public void setKol_16(String kol_16) {
        this.kol_16 = kol_16;
    }

    public String getKol_17() {
        return kol_17;
    }

    public void setKol_17(String kol_17) {
        this.kol_17 = kol_17;
    }

    public String getKol_18() {
        return kol_18;
    }

    public void setKol_18(String kol_18) {
        this.kol_18 = kol_18;
    }

    public String getKol_19() {
        return kol_19;
    }

    public void setKol_19(String kol_19) {
        this.kol_19 = kol_19;
    }

    public String getKol_20() {
        return kol_20;
    }

    public void setKol_20(String kol_20) {
        this.kol_20 = kol_20;
    }
}
