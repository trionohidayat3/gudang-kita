package com.android.sapxgudang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductIncomeAdapter extends RecyclerView.Adapter<ProductIncomeAdapter.ViewHolder> {
    List<ProductIncome> productList;
    Context context;

    public ProductIncomeAdapter(List<ProductIncome> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_product_incoming, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductIncome product = productList.get(position);
        holder.tv_no_masuk.setText(product.getNo_masuk());
        holder.tv_kode_produk.setText(product.getKode_produk());
        holder.tv_jumlah_produk.setText(product.getJumlah());
        holder.tv_tanggal_masuk.setText(product.getTanggal());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_no_masuk, tv_kode_produk, tv_jumlah_produk, tv_tanggal_masuk;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_no_masuk = itemView.findViewById(R.id.tv_no_masuk);
            tv_kode_produk = itemView.findViewById(R.id.tv_kode_produk);
            tv_jumlah_produk = itemView.findViewById(R.id.tv_jumlah_produk);
            tv_tanggal_masuk = itemView.findViewById(R.id.tv_tanggal_masuk);
        }
    }
}
