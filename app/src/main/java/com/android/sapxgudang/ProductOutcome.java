package com.android.sapxgudang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductOutcome {

    @SerializedName("no_order_pesanan")
    @Expose
    private String noOrderPesanan;
    @SerializedName("kode_produk")
    @Expose
    private String kodeProduk;
    @SerializedName("jumlah")
    @Expose
    private String jumlah;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;

    public String getNoOrderPesanan() {
        return noOrderPesanan;
    }

    public void setNoOrderPesanan(String noOrderPesanan) {
        this.noOrderPesanan = noOrderPesanan;
    }

    public String getKodeProduk() {
        return kodeProduk;
    }

    public void setKodeProduk(String kodeProduk) {
        this.kodeProduk = kodeProduk;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

}
