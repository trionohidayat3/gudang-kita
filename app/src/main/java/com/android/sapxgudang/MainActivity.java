package com.android.sapxgudang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.sapxgudang.fragment.AccountFragment;
import com.android.sapxgudang.fragment.ProductFragment;
import com.android.sapxgudang.fragment.StockFragment;
import com.android.sapxgudang.fragment.OrderFragment;
import com.android.sapxgudang.fragment.WarehouseFragment;
import com.google.android.material.navigation.NavigationBarView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;
    ViewPager viewPager;
    NavigationBarView bottomNavigationView;

    StockFragment fragStock;
    ProductFragment fragProduct;
    WarehouseFragment fragWare;
    OrderFragment fragOrder;
    AccountFragment fragAccount;

    MenuItem menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPrefManager = new SharedPrefManager(this);

        viewPager = findViewById(R.id.pager);
        setupViewPager(viewPager);

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnItemSelectedListener(menuItem -> {
            if (menuItem.getItemId() == R.id.menu1) {
                viewPager.setCurrentItem(0);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Kita Gudang");
            } else if (menuItem.getItemId() == R.id.menu2) {
                viewPager.setCurrentItem(1);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Product");
            } else if (menuItem.getItemId() == R.id.menu3) {
                viewPager.setCurrentItem(2);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Warehouse");
            } else if (menuItem.getItemId() == R.id.menu4) {
                viewPager.setCurrentItem(3);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Order");
            } else if (menuItem.getItemId() == R.id.menu5) {
                viewPager.setCurrentItem(4);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Account");
            } else {
                viewPager.setCurrentItem(0);
                Objects.requireNonNull(getSupportActionBar()).setTitle("SAPX Gudang");
            }

            return false;
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                menuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragStock = new StockFragment();
        fragProduct = new ProductFragment();
        fragWare = new WarehouseFragment();
        fragOrder = new OrderFragment();
        fragAccount = new AccountFragment();

        adapter.addFragment(fragStock);
        adapter.addFragment(fragProduct);
        adapter.addFragment(fragWare);
        adapter.addFragment(fragOrder);
        adapter.addFragment(fragAccount);
        viewPager.setAdapter(adapter);
    }
}