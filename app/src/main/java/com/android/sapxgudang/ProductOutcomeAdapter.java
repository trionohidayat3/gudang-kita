package com.android.sapxgudang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductOutcomeAdapter extends RecyclerView.Adapter<ProductOutcomeAdapter.ViewHolder> {

    List<ProductOutcome> productList;
    Context context;

    public ProductOutcomeAdapter(List<ProductOutcome> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_product_outcoming, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductOutcome productOutcome = productList.get(position);
        holder.tv_no_order_pesanan.setText(productOutcome.getNoOrderPesanan());
        holder.tv_kode_produk.setText(productOutcome.getKodeProduk());
        holder.tv_jumlah_produk.setText(productOutcome.getJumlah());
        holder.tv_tanggal_masuk.setText(productOutcome.getTanggal());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_no_order_pesanan, tv_kode_produk, tv_jumlah_produk, tv_tanggal_masuk;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_no_order_pesanan = itemView.findViewById(R.id.tv_no_order_pesanan);
            tv_kode_produk = itemView.findViewById(R.id.tv_kode_produk);
            tv_jumlah_produk = itemView.findViewById(R.id.tv_jumlah_produk);
            tv_tanggal_masuk = itemView.findViewById(R.id.tv_tanggal_masuk);
        }
    }

    public void clear() {
        productList.clear();
        notifyDataSetChanged();
    }
}
