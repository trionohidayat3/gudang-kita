package com.android.sapxgudang;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CardStockListAdapter extends RecyclerView.Adapter<CardStockListAdapter.ViewHolder> {
    List<CardStock> cardStockList;

    public CardStockListAdapter(List<CardStock> cardStockList) {
        this.cardStockList = cardStockList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_stock, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CardStock stock = cardStockList.get(position);
        holder.tv_stok_masuk.setText(stock.getMasuk());
        holder.tv_stok_keluar.setText(stock.getKeluar());
        holder.tv_stok_saldo.setText(stock.getSaldo());
        holder.tv_ref_no.setText(stock.getRefNo());
        holder.tv_tanggal.setText(stock.getTanggal());

        String stok_keluar = String.valueOf(holder.tv_stok_keluar.getText());
        String stok_masuk = String.valueOf(holder.tv_stok_masuk.getText());

        if (stok_keluar.equals("0")){
            holder.view_stock.setBackgroundColor(Color.parseColor("#9CFFAC"));
            holder.image_stock.setImageResource(R.drawable.in_box);
        } else if (stok_masuk.equals("0")){
            holder.view_stock.setBackgroundColor(Color.parseColor("#FFA68D"));
            holder.image_stock.setImageResource(R.drawable.out_box);
        } else {
            holder.view_stock.setBackgroundColor(Color.parseColor("#ADDCFF"));
            holder.image_stock.setImageResource(R.drawable.box);
        }
    }

    @Override
    public int getItemCount() {
        return cardStockList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View view_stock;
        ImageView image_stock;
        TextView tv_stok_masuk, tv_stok_keluar, tv_stok_saldo, tv_ref_no, tv_tanggal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view_stock = itemView.findViewById(R.id.view_stock);
            image_stock = itemView.findViewById(R.id.image_stock);
            tv_stok_masuk = itemView.findViewById(R.id.tv_stok_masuk);
            tv_stok_keluar = itemView.findViewById(R.id.tv_stok_keluar);
            tv_stok_saldo = itemView.findViewById(R.id.tv_stok_saldo);
            tv_ref_no = itemView.findViewById(R.id.tv_ref_no);
            tv_tanggal = itemView.findViewById(R.id.tv_tanggal);
        }
    }
}
