package com.android.sapxgudang.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.android.sapxgudang.PublicCom;
import com.android.sapxgudang.R;
import com.android.sapxgudang.model.PublicModels;

public class OrderPesananAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private final List<PublicModels> mitemsModelsl;
    //private List<PublicModels> itemsModelListFiltered;
    //private Context context;
    private final Activity mActivity;

    private final PublicCom mCommunicator;

    public OrderPesananAdapter(List<PublicModels> itemsModelsl, Activity activity,
                               RecyclerView recyclerView, PublicCom communication) {

        //mItemList = itemList;
        this.mitemsModelsl = itemsModelsl;
        //this.itemsModelListFiltered = itemsModelsl;
        this.mActivity = activity;
        mCommunicator=communication;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_order_pesanan, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        PublicModels singleGeneralDataModels= mitemsModelsl.get(position);

        ((ItemViewHolder) viewHolder).tv_Kol_01.setText(singleGeneralDataModels.getKol_01());
        ((ItemViewHolder) viewHolder).tv_kol_02.setText(singleGeneralDataModels.getKol_02());
        ((ItemViewHolder) viewHolder).tv_kol_03.setText(singleGeneralDataModels.getKol_03());
        ((ItemViewHolder) viewHolder).tv_kol_04.setText(singleGeneralDataModels.getKol_04());
        ((ItemViewHolder) viewHolder).tv_kol_05.setText(singleGeneralDataModels.getKol_05());
        if(singleGeneralDataModels.getKol_06().equals("1")){
            ((ItemViewHolder) viewHolder).yes_pickup.setVisibility(View.VISIBLE);
            ((ItemViewHolder) viewHolder).no_pickup.setVisibility(View.GONE);
        }else{
            ((ItemViewHolder) viewHolder).yes_pickup.setVisibility(View.GONE);
            ((ItemViewHolder) viewHolder).no_pickup.setVisibility(View.VISIBLE);
        }

        ((ItemViewHolder) viewHolder).item = singleGeneralDataModels;
    }
    @Override
    public int getItemCount() {
        return mitemsModelsl == null ? 0 : mitemsModelsl.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mitemsModelsl.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public PublicModels item;
        TextView tv_Kol_01,tv_kol_02,tv_kol_03,tv_kol_04,tv_kol_05;
        ImageView no_pickup,yes_pickup;
        //FragmentCommunication mComminication;
        public ItemViewHolder(View v) {
            super(v);
            tv_Kol_01 = v.findViewById(R.id.tv_Kol_01);
            tv_kol_02 = v.findViewById(R.id.tv_kol_02);
            tv_kol_03 = v.findViewById(R.id.tv_kol_03);
            tv_kol_04 = v.findViewById(R.id.tv_kol_04);
            tv_kol_05 = v.findViewById(R.id.tv_Kol_05);
            no_pickup = v.findViewById(R.id.no_pickup);
            yes_pickup = v.findViewById(R.id.yes_pickup);
        }
    }


}