package com.android.sapxgudang;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.android.sapxgudang.fragment.warehouse.WarehouseKeluarFragment;
import com.android.sapxgudang.fragment.warehouse.WarehouseMasukFragment;

public class WarehouseViewPagerAdapter extends FragmentPagerAdapter {

    public WarehouseViewPagerAdapter(
            @NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
            fragment = new WarehouseMasukFragment();
        else if (position == 1)
            fragment = new WarehouseKeluarFragment();

        assert fragment != null;
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
            title = "INCOMING";
        else if (position == 1)
            title = "OUTCOMING";

        return title;
    }
}
