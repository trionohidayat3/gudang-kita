package com.android.sapxgudang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("col_nama")
    @Expose
    private String colNama;
    @SerializedName("col_email")
    @Expose
    private String colEmail;
    @SerializedName("col_password")
    @Expose
    private String colPassword;

    public User(String colNama, String colEmail, String colPassword) {
        this.colNama = colNama;
        this.colEmail = colEmail;
        this.colPassword = colPassword;
    }

    public String getColNama() {
        return colNama;
    }

    public void setColNama(String colNama) {
        this.colNama = colNama;
    }

    public String getColEmail() {
        return colEmail;
    }

    public void setColEmail(String colEmail) {
        this.colEmail = colEmail;
    }

    public String getColPassword() {
        return colPassword;
    }

    public void setColPassword(String colPassword) {
        this.colPassword = colPassword;
    }

}