package com.android.sapxgudang;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    ArrayList<Product> productList;


    public ProductAdapter(ArrayList<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.kode_barang.setText(product.getKode_produk());
        holder.nama_barang.setText(product.getNama_produk());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView kode_barang, nama_barang;

        public ViewHolder(View itemView) {
            super(itemView);
            kode_barang = itemView.findViewById(R.id.txt_kode_barang);
            nama_barang = itemView.findViewById(R.id.txt_nama_barang);
        }
    }

    public void setFilter(ArrayList<Product> filterModel) {
        productList = filterModel;
        notifyDataSetChanged();
    }


}
