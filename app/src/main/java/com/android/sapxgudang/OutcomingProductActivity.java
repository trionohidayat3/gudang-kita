package com.android.sapxgudang;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.sapxgudang.activity.ScanActivity;
import com.android.sapxgudang.adapter.OrderPesananAdapter;
import com.android.sapxgudang.adapter.PublicAdapter;
import com.android.sapxgudang.api.APIClientDataPesanan;
import com.android.sapxgudang.api.APIgsonList;
import com.android.sapxgudang.api.ApiData_Order_Pesanan;
import com.android.sapxgudang.api.ApiData_Reseller;
import com.android.sapxgudang.async.AsyncBluetoothEscPosPrint;
import com.android.sapxgudang.async.AsyncEscPosPrinter;
import com.android.sapxgudang.model.PublicModels;
import com.android.sapxgudang.utils.ClickListener;
import com.android.sapxgudang.utils.RecyclerClickListener;
import com.android.sapxgudang.utils.Server;
import com.dantsu.escposprinter.connection.DeviceConnection;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




public class OutcomingProductActivity extends AppCompatActivity {

    MaterialCardView pilih_kurir, btn_scan, btn_sign, btn_print;

    private BottomSheetBehavior<View> mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    private ApiData_Order_Pesanan mApiData_Order_Pesanan;

    private RecyclerView mRV_OrderPenjualan;
    LinearLayoutManager mLinearLayoutManager_OrderPenjualan;
    private OrderPesananAdapter mPublicAdapter_OrderPenjualan;

    private static List<PublicModels> dataListData_OrderPenjualan;

    private RecyclerView mRV_ekspedisi;
    private PublicAdapter mPublicAdapter_ekspedisi;

    private static List<PublicModels> dataListData_ekspedisi;

    private ApiData_Reseller mApiData_Reseller;
    private TextView tv_ekspedisi;

    int adapter_position_public = 0;
    PublicCom communication_public = new PublicCom() {
        @Override
        public void respond(int position, String kol_01) {
            if (position >= 0) {
                adapter_position_public = position;
                tv_ekspedisi.setText(dataListData_ekspedisi.get(position).getKol_01());
                mBottomSheetDialog.hide();

                populateDataOrderPenjualan(dataListData_ekspedisi.get(position).getKol_01());

            }
        }
    };

    Bitmap bitmap;

    ImageView img_signature;
    String namalengkap, keterangan;
    TextView tv_namalengkap, tv_keterangan;

    public static final int PERMISSION_BLUETOOTH = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outcoming_product);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Outcoming Product");

        mRV_OrderPenjualan = findViewById(R.id.mRV_OrderPenjualan);
        dataListData_OrderPenjualan = new ArrayList<>();

        pilih_kurir = findViewById(R.id.pilih_kurir);
        pilih_kurir.setOnClickListener(view -> showBottomSheetDialog());

        tv_ekspedisi = findViewById(R.id.tv_ekspedisi);

        tv_namalengkap = findViewById(R.id.tv_namalengkap);
        tv_keterangan = findViewById(R.id.tv_keterangan);
        img_signature = findViewById(R.id.img_signature);

        img_signature.setImageResource(0);

        ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Handle the returned result
                        Intent data = result.getData();
                        Bundle bundle = data.getExtras();

                        String status = bundle.getString("status");

                        if (status.equalsIgnoreCase("done")) {
                            Toast.makeText(OutcomingProductActivity.this, "Signature saved to gallery.", Toast.LENGTH_SHORT).show();
                            String path_image = bundle.getString("image");
                            namalengkap = bundle.getString("namalengkap");
                            keterangan = bundle.getString("keterangan");
                            tv_namalengkap.setText(namalengkap);
                            tv_keterangan.setText(keterangan);

                            Uri filePath = Uri.parse(path_image);
                            try {
                                //Getting the Bitmap from Gallery
                                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                                //Setting the Bitmap to ImageView
                                img_signature.setImageBitmap(bitmap);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            namalengkap = "";
                            keterangan = "";
                            tv_namalengkap.setText(namalengkap);
                            tv_keterangan.setText(keterangan);
                            img_signature.setImageResource(0);
                            img_signature.setImageBitmap(null);
                            img_signature.destroyDrawingCache();

                        }
                    } else {
                        keterangan = "";
                        tv_namalengkap.setText(namalengkap);
                        tv_keterangan.setText(keterangan);
                        img_signature.setImageResource(0);
                        img_signature.setImageBitmap(null);
                        img_signature.destroyDrawingCache();
                    }
                });

        btn_scan = findViewById(R.id.btn_scan);
        btn_scan.setOnClickListener(view -> startActivity(new Intent(this, ScanActivity.class)));

//        btn_sign = findViewById(R.id.btn_sign);
//        btn_sign.setOnClickListener(view -> {
//            namalengkap = "";
//            keterangan = "";
//            Intent intent = new Intent(getApplicationContext(), SignatureActivity.class);
//            resultLauncher.launch(intent);
//        });

        btn_print = findViewById(R.id.btn_print);
        btn_print.setOnClickListener(view -> {
            Toast.makeText(this, "Tombol Print Sukses", Toast.LENGTH_SHORT).show();

            if (dataListData_OrderPenjualan.size() > 0) {

                printBluetooth();

            }

        });

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        mApiData_Reseller = APIClientDataPesanan.getClient().create(ApiData_Reseller.class);

        mApiData_Order_Pesanan = APIClientDataPesanan.getClient().create(ApiData_Order_Pesanan.class);
    }

    private void showBottomSheetDialog() {
        namalengkap = "";
        keterangan = "";
        tv_namalengkap.setText(namalengkap);
        tv_keterangan.setText(keterangan);
        img_signature.setImageResource(0);
        img_signature.setImageBitmap(null);
        img_signature.destroyDrawingCache();

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        final View viewer = getLayoutInflater().inflate(R.layout.layout_bottom_kurir, null);
        (viewer.findViewById(R.id.bt_close)).setOnClickListener(view1 -> mBottomSheetDialog.hide());


        mRV_ekspedisi = viewer.findViewById(R.id.mRV_ekspedisi);
        mRV_ekspedisi.setHasFixedSize(true);
        dataListData_ekspedisi = new ArrayList<>();
        mPublicAdapter_ekspedisi = new PublicAdapter(dataListData_ekspedisi, this,
                mRV_ekspedisi, communication_public);
        mRV_ekspedisi.addOnItemTouchListener(new RecyclerClickListener(this,
                mRV_ekspedisi, new ClickListener() {
            @Override
            public void OnClick(View view, int posisi) {
            }

            @Override
            public void onLongClick(View view, int posisi) {
            }
        }));
        populateDataEkspedisi();


        mBottomSheetDialog = new BottomSheetDialog(this, R.style.DialogStyle);
        mBottomSheetDialog.setContentView(viewer);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        ((View) viewer.getParent()).setBackgroundColor(Color.TRANSPARENT);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }


    private void populateDataEkspedisi() {
        if (dataListData_ekspedisi.size() > 0) {
            dataListData_ekspedisi.clear();
        }
        Call<APIgsonList> call_get = mApiData_Reseller.doGetData_Pengirim(Server.DB_OTP, "");
        call_get.enqueue(new Callback<APIgsonList>() {
            @Override
            public void onResponse(Call<APIgsonList> call, Response<APIgsonList> response) {
                Log.v("RESPONSE_CALLED", "ON_RESPONSE_CALLED");
                String didItWork = String.valueOf(response.isSuccessful());
                Log.v("SUCCESS?", didItWork);
                Log.v("RESPONSE_CODE", String.valueOf(response.code()));
                if (response.isSuccessful()) {
                    Log.v("RESPONSE_BODY", String.valueOf(response.body()));
                    APIgsonList apigsonList = response.body();
                    Integer success = apigsonList.success;

                    List<APIgsonList.Datum> datumList = apigsonList.data;
                    if (success == 1) {
                        int i = 0;
                        int norut = 0;
                        for (APIgsonList.Datum datum : datumList) {
                            norut = norut + 1;
                            dataListData_ekspedisi.add(new PublicModels(norut,
                                    datum.id,
                                    datum.kol_01,
                                    datum.kol_02,
                                    datum.kol_03,
                                    datum.kol_04,
                                    datum.kol_05,
                                    datum.kol_06,
                                    datum.kol_07,
                                    datum.kol_08,
                                    datum.kol_09,
                                    datum.kol_10,
                                    "",
                                    "",
                                    "", "",
                                    "", "", "", "", "", ""
                            ));
                            i++;
                        }
                    }
                    mPublicAdapter_ekspedisi.notifyItemRangeInserted(dataListData_ekspedisi.size() - 1, dataListData_ekspedisi.size());
                    mRV_ekspedisi.setAdapter(mPublicAdapter_ekspedisi);

                } else {
                    Log.v("RESPONSE_ERORR", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<APIgsonList> call, Throwable t) {
                Log.v("RESPONSE_onFailure", String.valueOf(t));
                call.cancel();
            }
        });
    }

    private void populateDataOrderPenjualan(String strexpedisi) {
        if (dataListData_OrderPenjualan.size() > 0) {
            dataListData_OrderPenjualan.clear();
        }

        Call<APIgsonList> call_get = mApiData_Order_Pesanan.doGetByexpedisi(Server.DB_OTP,
                "", "", strexpedisi);

        call_get.enqueue(new Callback<APIgsonList>() {
            @Override
            public void onResponse(Call<APIgsonList> call, Response<APIgsonList> response) {
                Log.v("RESPONSE_CALLED", "ON_RESPONSE_CALLED");
                String didItWork = String.valueOf(response.isSuccessful());
                Log.v("SUCCESS?", didItWork);
                Log.v("RESPONSE_CODE", String.valueOf(response.code()));
                if (response.isSuccessful()) {
                    Log.v("RESPONSE_BODY", String.valueOf(response.body()));
                    APIgsonList apigsonList = response.body();
                    Integer success = apigsonList.success;
                    String message = apigsonList.message;

                    List<APIgsonList.Datum> datumList = apigsonList.data;
                    if (success == 1) {
                        int i = 0;
                        int norut = 0;
                        for (APIgsonList.Datum datum : datumList) {
                            norut = norut + 1;
                            dataListData_OrderPenjualan.add(new PublicModels(norut,
                                    datum.id,
                                    datum.kol_01,
                                    datum.kol_02,
                                    datum.kol_03,
                                    datum.kol_04,
                                    datum.kol_05,
                                    "0",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "", "",
                                    "", "", "", "", "", ""
                            ));

                            i++;
                        }
                    }


                    mRV_OrderPenjualan.setHasFixedSize(true);
                    mLinearLayoutManager_OrderPenjualan = new LinearLayoutManager(OutcomingProductActivity.this);
                    mPublicAdapter_OrderPenjualan = new OrderPesananAdapter(dataListData_OrderPenjualan, OutcomingProductActivity.this,
                            mRV_OrderPenjualan, communication_public);
                    mPublicAdapter_OrderPenjualan.notifyItemRangeInserted(dataListData_OrderPenjualan.size() - 1, dataListData_OrderPenjualan.size());
                    mRV_OrderPenjualan.setAdapter(mPublicAdapter_OrderPenjualan);

                } else {
                    Log.v("RESPONSE_ERORR", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<APIgsonList> call, Throwable t) {
                Log.v("RESPONSE_onFailure", String.valueOf(t));
                call.cancel();
            }
        });
    }


    public void printBluetooth() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH}, OutcomingProductActivity.PERMISSION_BLUETOOTH);
        } else {
            // this.printIt(BluetoothPrintersConnections.selectFirstPaired());
            // new AsyncBluetoothEscPosPrint(this).execute(this.getAsyncEscPosPrinter(null));
            new AsyncBluetoothEscPosPrint(this).execute(this.getAsyncEscPosPrinterPOS(null));
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public AsyncEscPosPrinter getAsyncEscPosPrinterPOS(DeviceConnection printerConnection) {
        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 48f, 32);

        //GET PERUSAHAAAN
        String com_companyname = "34.12503";
        String com_address = "SPBU RAYA PASAR MINGGU";
        String com_kabupaten = "JL.RAYA PASAR MINGGU";
        String com_kecamatan = "Shift : 1  No.Trans: 1522732";
        String com_telepon = "Waktu : 20/01/2022  14:30:10";
        String com_fax = "021";
        String com_email = "kitakirim.id";
        String com_contact = "Vinsen";
        String prtNoPOS = "";
        String strTextPrint = "NO DATA TO PRINT";
        //String dateprint=format.format(new Date());
        String dateprint = format.format(new Date());


        strTextPrint = "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer,
                this.getApplicationContext().getResources().getDrawableForDensity(R.mipmap.ic_launcher,
                        DisplayMetrics.DENSITY_XHIGH)) + "</img>\n";

        strTextPrint = strTextPrint + "[C]<font size='big'>Gudang Alkira Fashion</font>\n" +
                "[L]<font size='small'>Tanda Terima</font>\n" +
                "[L]<font size='small'>JL.RAYA PASAR MINGGU</font>\n" +
                "[L]<font size='small'>No.Invoice : " + "txt_nopesanan.getText().toString()" + "</font>\n" +
                "[L]<font size='small'>Waktu : 27/01/2022   18:30:25</font>\n" +
                "[L]<font size='small'>Diberikan : Nasrullah</font>\n" +
                "[L]===============================================\n";

        for (int i = 0; i < dataListData_OrderPenjualan.size(); i++) {
            //dataDataTokoList.get(i).getKol_02();
            strTextPrint = strTextPrint + "[L]Nomor Resi  :[R]" + dataListData_OrderPenjualan.get(i).getKol_01() + "\n";
            strTextPrint = strTextPrint + "[L]Produk      :[R]" + dataListData_OrderPenjualan.get(i).getKol_02() + "\n";
            strTextPrint = strTextPrint + "[C]-----------------------------------------------\n";
        }
        strTextPrint = strTextPrint + "[L]===============================================\n";

        strTextPrint = strTextPrint + "[L]<font size='tall'>" + tv_keterangan.getText().toString() + "</font>" + "\n";
        strTextPrint = strTextPrint + "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, getResizedBitmap(bitmap, 200)) + "</img>\n";
        strTextPrint = strTextPrint + "[L]<b>Diterima Kurir   :[R]" + tv_namalengkap.getText().toString() + "</b>\n";
        strTextPrint = strTextPrint + "[L]<font size='tall'>TERIMA KASIH </font>" + "\n";
        strTextPrint = strTextPrint + "[L]\n";


        return printer.setTextToPrint(strTextPrint);
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}