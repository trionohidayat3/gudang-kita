package com.android.sapxgudang.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.CardStock;
import com.android.sapxgudang.CardStockListAdapter;
import com.android.sapxgudang.DetailStock;
import com.android.sapxgudang.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailStockActivity extends AppCompatActivity {

    TextView text_nama_produk, text_stok, text_tanggal;
    SwipeRefreshLayout swiperefresh_layout;
    RecyclerView rv_stok_detail;
    CardStockListAdapter adapter;

    List<CardStock> cardStockList;

    List<DetailStock> detailStockList;

    API_Product api_product;

//    DetailStock detailStock;
    String nama_produk, kode_produk, stok_produk, tanggal_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_stock);

//        detailStock = new DetailStock();

        Intent intent = getIntent();
//        nama_produk = detailStock.getNamaProduk();
        kode_produk = intent.getStringExtra("KODE_PRODUK");
//        stok_produk = detailStock.getSaldo();
//        tanggal_update = detailStock.getTanggal();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Kode Produk: " + kode_produk);

        text_nama_produk = findViewById(R.id.text_nama_produk);
        text_stok = findViewById(R.id.text_stok);
        text_tanggal = findViewById(R.id.text_tanggal);

        text_nama_produk.setText(nama_produk);
        text_stok.setText(stok_produk);
        text_tanggal.setText(tanggal_update);

        swiperefresh_layout = findViewById(R.id.swiperefresh_layout);
        swiperefresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swiperefresh_layout.setOnRefreshListener(() -> {
            swiperefresh_layout.setRefreshing(false);
            loaddata();
        });

        rv_stok_detail = findViewById(R.id.rv_stok_detail);
        rv_stok_detail.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_stok_detail.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        api_product = API_Client.getClientProduct().create(API_Product.class);

//        Call<List<DetailStock>> callDetail = api_product.view_specific_stock_product(kode_produk);

        loaddata();

    }

    private void loaddata() {
        Call<List<CardStock>> call = api_product.view_specific_stock(kode_produk);

        call.enqueue(new Callback<List<CardStock>>() {
            @Override
            public void onResponse(@NonNull Call<List<CardStock>> call, @NonNull Response<List<CardStock>> response) {
                cardStockList = response.body();

                adapter = new CardStockListAdapter(cardStockList);
                rv_stok_detail.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<CardStock>> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}