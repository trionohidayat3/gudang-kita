package com.android.sapxgudang.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.R;
import com.android.sapxgudang.SharedPrefManager;
import com.android.sapxgudang.User;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    TextInputEditText edtNama, edtEmail, edtPass;
    MaterialCardView btnRegister;

    String nama, email, password;

    ArrayList<User> userArrayList;
    API_Product api_product;


    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Daftar");

        sharedPrefManager = new SharedPrefManager(this);

        edtNama = findViewById(R.id.input_name);
        edtEmail = findViewById(R.id.input_email);
        edtPass = findViewById(R.id.input_password);

        btnRegister = findViewById(R.id.card_daftar);

        btnRegister.setOnClickListener(view -> {

            nama = edtNama.getText().toString();
            email = edtEmail.getText().toString();
            password = edtPass.getText().toString();

            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAME, nama);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, email);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_PASSWORD, password);

            api_product = API_Client.getClientProduct().create(API_Product.class);
            Call<ResponseBody> call = api_product.insert_user(nama, email, password);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Toast.makeText(RegisterActivity.this, "Sukses", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}