package com.android.sapxgudang.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.MainActivity;
import com.android.sapxgudang.R;
import com.android.sapxgudang.SharedPrefManager;


import com.android.sapxgudang.User;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText input_email, input_password;
    MaterialCardView card_masuk;
    String email, password;


    API_Product api_product;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Login");


        sharedPrefManager = new SharedPrefManager(this);

        input_email = findViewById(R.id.input_email);
        input_password = findViewById(R.id.input_password);

        card_masuk = findViewById(R.id.card_masuk);
        card_masuk.setOnClickListener(view -> {
            email = Objects.requireNonNull(input_email.getText()).toString();
            password = Objects.requireNonNull(input_password.getText()).toString();

            api_product = API_Client.getClientProduct().create(API_Product.class);

            Call<List<User>> call = api_product.login_user(email, password);

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                    if (response.body().isEmpty()) {
                        Toast.makeText(LoginActivity.this, "Mohon coba lagi", Toast.LENGTH_SHORT).show();
                    } else {
                        sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, email);
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
//                    if (userResponse != null) {
//                        String sName = userResponse.getColNama();
////                    List<User user = (List<User) response.body();
////                    userList = response.body().toString();
////                    String sName = user.getColEmail();
//
////                        Toast.makeText(LoginActivity.this, sName, Toast.LENGTH_SHORT).show();
//                    }


//                    try {
//                        assert response.body() != null;
//                        String responseRecieved = response.body().string();
//
//                        Toast.makeText(LoginActivity.this, responseRecieved, Toast.LENGTH_SHORT).show();
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }


//                    if (response.isSuccessful()) {
//                        String email = response.body().getColEmail();
//                        Toast.makeText(LoginActivity.this, email, Toast.LENGTH_SHORT).show();
//
//                    }


//                    try {
//                        assert response.body() != null;
//                        if (response.body().string().isEmpty()) {
//                            Toast.makeText(LoginActivity.this, "Coba Lagi", Toast.LENGTH_SHORT).show();
//                        } else {
//                            sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, email);
//                            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
//
//                            response.body();
//
//                            String nama = response.body().getColNama();
//                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                            startActivity(intent);
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {

                }
            });
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}