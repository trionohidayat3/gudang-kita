package com.android.sapxgudang.activity;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.sapxgudang.R;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScanActivity extends AppCompatActivity {


    public final String TAG = "ScanActivity";
    public String photoFileName = "photo.jpg";
    File photoFile;

    TextInputLayout txt_layout_scan;
    TextInputEditText txt_result;
    ImageView imageView;
    MaterialCardView photoButton, btn_simpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Scan");

        photoFileName = new SimpleDateFormat("yyyyMMdd_hhmmss").format(new Date()) + ".jpg";

        txt_layout_scan = findViewById(R.id.txt_layout_scan);
        txt_layout_scan.setEndIconOnClickListener(view -> {
            //BUAT ACTVITY SCAN BARCODE
            IntentIntegrator integrator = new IntentIntegrator(ScanActivity.this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setCaptureActivity(ContinuousScanActivity.class);
            integrator.setOrientationLocked(false);
            integrator.setPrompt("Process Scaning Barcode");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(true);
            integrator.initiateScan();

        });
        txt_result = findViewById(R.id.txt_result);
        txt_result.requestFocus();
        txt_result.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0){
                    photoButton.setCardBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                } else {
                    photoButton.setCardBackgroundColor(getResources().getColor(R.color.primaryColor));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        imageView = findViewById(R.id.imageView1);

        ActivityResultLauncher<Intent> resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        if (photoFile != null) {
                            Uri selectedImage = FileProvider.getUriForFile(ScanActivity.this, getPackageName() + ".fileprovider", photoFile);
                            getContentResolver().notifyChange(selectedImage, null);
                            Bitmap reducedSizeBitmap = getBitmap(photoFile.getAbsolutePath());

                            if (reducedSizeBitmap != null) {
                                imageView.setImageBitmap(reducedSizeBitmap);
                                btn_simpan.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(ScanActivity.this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(ScanActivity.this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        photoButton = findViewById(R.id.btn_take_photo);
        photoButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(txt_result.getText())) {
                Toast.makeText(ScanActivity.this, "Please Scaning First", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoFile = getPhotoFileUri(photoFileName);

                Uri fileProvider = FileProvider.getUriForFile(ScanActivity.this, getPackageName() + ".fileprovider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);
                resultLauncher.launch(intent);


            }

        });

        btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(v -> {

            Bundle b = new Bundle();
            b.putString("qrcode_result", txt_result.getText().toString());
            Intent intent = new Intent();
            intent.putExtras(b);
            setResult(RESULT_OK, intent);
            finish();

        });


//        scanButton = findViewById(R.id.btn_scanning);
//        scanButton.setOnClickListener(v -> {
//            //BUAT ACTVITY SCAN BARCODE
//            IntentIntegrator integrator = new IntentIntegrator(ScanActivity.this);
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//            integrator.setCaptureActivity(ContinuousScanActivity.class);
//            integrator.setOrientationLocked(false);
//            integrator.setPrompt("Process Scaning Barcode");
//            integrator.setCameraId(0);
//            integrator.setBeepEnabled(true);
//            integrator.setBarcodeImageEnabled(true);
//            integrator.initiateScan();
//
//        });

    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), TAG);

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(TAG, "failed to create directory");
        }

        File file = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return file;
    }

    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_LONG).show();
            } else {
                txt_result.setText(result.getContents());
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putString("qrcode_result", txt_result.getText().toString());
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
    }
}