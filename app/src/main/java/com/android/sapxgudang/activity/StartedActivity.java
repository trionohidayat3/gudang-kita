package com.android.sapxgudang.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.android.sapxgudang.MainActivity;
import com.android.sapxgudang.R;
import com.android.sapxgudang.SharedPrefManager;
import com.google.android.material.card.MaterialCardView;

import java.util.Objects;

public class StartedActivity extends AppCompatActivity {

    MaterialCardView card_daftar, card_masuk;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_started);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().hide();

        sharedPrefManager = new SharedPrefManager(this);

        if (sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        card_daftar = findViewById(R.id.card_daftar);
        card_daftar.setOnClickListener(view -> startActivity(new Intent(this, RegisterActivity.class)));

        card_masuk = findViewById(R.id.card_masuk);
        card_masuk.setOnClickListener(view -> startActivity(new Intent(this, LoginActivity.class)));
    }
}