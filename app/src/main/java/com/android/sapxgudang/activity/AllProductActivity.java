package com.android.sapxgudang.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.sapxgudang.API_Client;
import com.android.sapxgudang.API_Product;
import com.android.sapxgudang.CardStock;
import com.android.sapxgudang.CardStockGridAllProductAdapter;
import com.android.sapxgudang.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllProductActivity extends AppCompatActivity {


    RecyclerView rv_all_product;
    RecyclerView.LayoutManager layoutManager;
    List<CardStock> cardStockList;
    CardStockGridAllProductAdapter adapter;

    API_Product api_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_product);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("All Product");

        rv_all_product = findViewById(R.id.rv_all_product);
        rv_all_product.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 3);
        rv_all_product.setLayoutManager(layoutManager);

        api_product = API_Client.getClientProduct().create(API_Product.class);

        Call<List<CardStock>> call = api_product.view_card_stock();
        call.enqueue(new Callback<List<CardStock>>() {
            @Override
            public void onResponse(@NonNull Call<List<CardStock>> call, @NonNull Response<List<CardStock>> response) {
                cardStockList = response.body();
                adapter = new CardStockGridAllProductAdapter(cardStockList, AllProductActivity.this);
                rv_all_product.setAdapter(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<CardStock>> call, @NonNull Throwable t) {

            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}