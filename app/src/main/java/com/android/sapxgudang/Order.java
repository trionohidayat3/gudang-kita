package com.android.sapxgudang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("no_order_pesanan")
    @Expose
    private String noOrderPesanan;

    @SerializedName("kode_produk")
    @Expose
    private String kodeProduk;

    @SerializedName("jumlah")
    @Expose
    private String jumlah;

    @SerializedName("tanggal")
    @Expose
    private String tanggal;

    @SerializedName("expedisi")
    @Expose
    private String expedisi;

    @SerializedName("no_resi")
    @Expose
    private String noResi;

    public String getNoOrderPesanan() {
        return noOrderPesanan;
    }

    public void setNoOrderPesanan(String noOrderPesanan) {
        this.noOrderPesanan = noOrderPesanan;
    }

    public String getKodeProduk() {
        return kodeProduk;
    }

    public void setKodeProduk(String kodeProduk) {
        this.kodeProduk = kodeProduk;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getExpedisi() {
        return expedisi;
    }

    public void setExpedisi(String expedisi) {
        this.expedisi = expedisi;
    }

    public String getNoResi() {
        return noResi;
    }

    public void setNoResi(String noResi) {
        this.noResi = noResi;
    }

}
