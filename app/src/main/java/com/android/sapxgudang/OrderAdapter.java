package com.android.sapxgudang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    List<Order> orderList;
    Context mContext;

    public OrderAdapter(List<Order> orderList, Context mContext) {
        this.orderList = orderList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_row_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Order order = orderList.get(position);
        holder.tv_kode_produk.setText(order.getKodeProduk());
        holder.tv_jumlah.setText(order.getJumlah());
        holder.tv_tanggal.setText(order.getTanggal());
        holder.tv_expedisi.setText(order.getExpedisi());
        holder.tv_no_resi.setText(order.getNoResi());
        holder.tv_no_order.setText(order.getNoOrderPesanan());
        holder.itemView.setOnClickListener(view -> Toast.makeText(mContext, "Klik " + order.getNoOrderPesanan(), Toast.LENGTH_SHORT).show());
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tv_kode_produk, tv_jumlah, tv_tanggal, tv_expedisi, tv_no_resi, tv_no_order;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_kode_produk = itemView.findViewById(R.id.tv_kode_produk);
            tv_jumlah = itemView.findViewById(R.id.tv_jumlah);
            tv_tanggal = itemView.findViewById(R.id.tv_tanggal);
            tv_expedisi = itemView.findViewById(R.id.tv_expedisi);
            tv_no_resi = itemView.findViewById(R.id.tv_no_resi);
            tv_no_order = itemView.findViewById(R.id.tv_no_order);
        }
    }

}
